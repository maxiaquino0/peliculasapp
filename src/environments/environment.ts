// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCZIHGGTo5gcibyxWlutu5Xj8iMGJC2VdU',
    authDomain: 'peliculasapp-893a1.firebaseapp.com',
    databaseURL: 'https://peliculasapp-893a1.firebaseio.com',
    projectId: 'peliculasapp-893a1',
    storageBucket: 'peliculasapp-893a1.appspot.com',
    messagingSenderId: '408812815420',
    appId: '1:408812815420:web:a362f49be022010a506f39',
    measurementId: 'G-5D2ZR8FT1H'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
