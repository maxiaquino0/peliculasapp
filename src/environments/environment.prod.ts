export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCZIHGGTo5gcibyxWlutu5Xj8iMGJC2VdU',
    authDomain: 'peliculasapp-893a1.firebaseapp.com',
    databaseURL: 'https://peliculasapp-893a1.firebaseio.com',
    projectId: 'peliculasapp-893a1',
    storageBucket: 'peliculasapp-893a1.appspot.com',
    messagingSenderId: '408812815420',
    appId: '1:408812815420:web:a362f49be022010a506f39',
    measurementId: 'G-5D2ZR8FT1H'
  }
};
