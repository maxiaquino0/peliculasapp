import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PeliculasService } from 'src/app/services/peliculas.service';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {

  pelicula = null;
  pag = '';

  constructor(private activatedRoute: ActivatedRoute, private peliculasService: PeliculasService, private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      params => {
        this.pag = params.pag;
        this.peliculasService.getDetallePelicula(params.id).subscribe(
          (resp: any) => {
            this.pelicula = resp;
          }
        );
      }
    );
  }

  regresar() {
    return;
    // if (this.peliculasService.getBackToSearch() === '') {
    //   this.router.navigate([ '/home']);
    // } else {
    //   this.router.navigate([ '/busqueda']);
    // }
  }

}
