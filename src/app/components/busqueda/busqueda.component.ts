import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PeliculasService } from 'src/app/services/peliculas.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {

  query = '';
  peliculas = [];

  constructor(public peliculasService: PeliculasService, private router: Router, public activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(
      params => {
        if (params.texto) {
          this.query = params.texto;
          this.buscarPelicula();
        }
      }
    );
  }

  ngOnInit() {
  }

  buscarPelicula() {
    if (this.query.length === 0) {
      return;
    }

    this.peliculasService.buscarPelicula(this.query).subscribe(
      (resp: any) => {
        this.peliculas = resp;
      }
    );
  }
}
