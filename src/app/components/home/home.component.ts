import { Component, OnInit } from '@angular/core';
import { PeliculasService } from 'src/app/services/peliculas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  peliculascartelera = [];
  peliculaspopulares = [];
  peliculaspopularesninos = [];

  constructor(private peliculasService: PeliculasService, private router: Router) {
  }

  ngOnInit() {
    this.peliculasService.getCarteleraHoy().subscribe(
      resp => {
        this.peliculascartelera = resp;
      }
    );

    this.peliculasService.getPopulares().subscribe(
      resp => {
        this.peliculaspopulares = resp;
      }
    );

    this.peliculasService.getPopularesNinos().subscribe(
      resp => {
        this.peliculaspopularesninos = resp;
      }
    );
  }

  

}
