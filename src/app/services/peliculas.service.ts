import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PeliculasService {

  private apikey: string = '50af2b2651299e17182afa2f31f02bda';
  private urlMovieDb: string = 'https://api.themoviedb.org/3';

  peliculas: any = [];

  constructor(private httpClient: HttpClient) { }

  getCarteleraHoy() {
    const desde = new Date();
    const hasta = new Date();
    hasta.setDate(hasta.getDate() + 7);

    const desdeStr = `${desde.getFullYear()}-${ (desde.getMonth() + 1 < 10) ? '0' + (desde.getMonth() + 1).toString() : (desde.getMonth() + 1).toString() }-${ (desde.getDate() < 10) ? '0' + desde.getDate().toString() : desde.getDate().toString() }`;
    const hastaStr = `${hasta.getFullYear()}-${ (hasta.getMonth() + 1 < 10) ? '0' + (hasta.getMonth() + 1).toString() : (hasta.getMonth() + 1).toString() }-${ (hasta.getDate() < 10) ? '0' + hasta.getDate().toString() : hasta.getDate().toString() }`;
    
    let url = `${this.urlMovieDb}/discover/movie?api_key=${this.apikey}&language=es&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&primary_release_date.gte=${desdeStr}&primary_release_date.lte=${hastaStr}`;
    return this.httpClient.get(url).pipe(
      map((res: any) => res.results)
    );
  }

  getPopulares() {
    let url = `${this.urlMovieDb}/discover/movie?api_key=${this.apikey}&language=es&sort_by=popularity.desc&include_adult=false&include_video=false&page=1`;
    return this.httpClient.get(url).pipe(
      map((res: any) => res.results)
    );
  }

  getPopularesNinos() {
    let url = `${this.urlMovieDb}/discover/movie?api_key=${this.apikey}&language=es&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&certification.lte=G`;
    return this.httpClient.get(url).pipe(
      map((res: any) => res.results)
    );
  }

  getDetallePelicula(id: number) {
    let url = `${this.urlMovieDb}/movie/${id}?api_key=${this.apikey}&language=es`;
    return this.httpClient.get(url);
  }

  buscarPelicula(query: string) {
    let url = `${this.urlMovieDb}/search/movie?api_key=${this.apikey}&language=es&query=${query}&page=1&include_adult=true`;
    return this.httpClient.get(url).pipe(
      map((res: any) => {
        this.peliculas = res.results;
        return res.results;
      })
    );
  }
}
