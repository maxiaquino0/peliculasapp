import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public usuario: any = {
    nombre: '',
    uid: '',
    photoURL: ''
  };

  constructor(public afAuth: AngularFireAuth, private router: Router) {
    afAuth.authState.subscribe(
      user => {
        if (!user) {
          return;
        }
        this.usuario.nombre = user.displayName;
        this.usuario.uid = user.uid;
        this.usuario.photoURL = user.photoURL;
      }
    );
  }

  login(proveedor: string) {
    if (proveedor === 'google') {
      return this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    } else  {
      return this.afAuth.auth.signInWithPopup(new auth.TwitterAuthProvider());
    }
  }
  logout() {
    this.usuario = {
      nombre: '',
      uid: '',
      photoURL: ''
    };
    this.afAuth.auth.signOut();
    this.router.navigate(['home']);
  }
}
